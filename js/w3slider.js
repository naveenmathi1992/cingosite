var slideIndex = 0;
showDivs(slideIndex);

function plusDivs(n) {
    showDivs(slideIndex += n);
}

function currentDiv(n) {
    showDivs(slideIndex = n);
}

autoSlideShow();
function autoSlideShow(){
    setTimeout(autoSlideShow, 4000);
    showDivs(slideIndex += 1);
}

function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("demo");
    if (n > x.length) {slideIndex = 1}
    if (n < 1) {slideIndex = x.length}
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    var currentdot;
    for (i = 0; i < dots.length; i++) {
        if (dots[i].className.includes(" w3-white")) {
            currentdot = i;
            dots[i].className = dots[i].className.replace(" w3-white", "");
        }
    }
    if (currentdot < (slideIndex - 1)) {
        x[slideIndex - 1].className = x[slideIndex - 1].className.replace(" w3-animate-left", "");
        x[slideIndex - 1].className = x[slideIndex - 1].className.replace(" w3-animate-right", "");
        x[slideIndex - 1].className += " w3-animate-right";
    }else{
        x[slideIndex - 1].className = x[slideIndex - 1].className.replace(" w3-animate-right", "");
        x[slideIndex - 1].className = x[slideIndex - 1].className.replace(" w3-animate-left", "");
        x[slideIndex - 1].className += " w3-animate-left";
    }

    x[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " w3-white";
}